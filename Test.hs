module Test where

import Record
import Data.List

runJSONTest :: FilePath -> Record -> IO ()
runJSONTest file r = do
    writeRecord file r
    d <- readRecord file
    case d of
        Left err -> putStrLn err
        Right ps -> putStrLn $ intercalate "\n\n" (map show ps)
