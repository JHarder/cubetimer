{-# LANGUAGE DeriveGeneric #-}

module Record where

import qualified Data.ByteString.Lazy as B
import GHC.Generics
import Data.Aeson
import Data.Maybe (listToMaybe)
import Control.Applicative ((<$>))
import Control.Monad
import System.Random (randomRIO)

data Move = R | R' | R2 | U | U' | U2
          | L | L' | L2 | D | D' | D2
          | F | F' | F2 | B | B' | B2 deriving (Show, Enum, Generic)

type Scramble = [Move]

data Record = Record { time :: Float,
                       number :: Int,
                       scramble :: Scramble }
    deriving Generic

instance Show Record where
    show (Record _time _number _scramble) =
        "solve number: " ++ show _number ++ "\n" ++
        "time: " ++ show _time ++ "\n" ++
        "scramble: " ++ showScramble _scramble ++ "\n" ++
        "=================================================="

instance FromJSON Record
instance ToJSON Record

instance FromJSON Move
instance ToJSON Move

showScramble :: Scramble -> String
showScramble = unwords . map show

genMove :: IO Move
genMove = toEnum <$> randomRIO (0,11)

genScramble :: Int -> IO [Move]
genScramble = flip replicateM genMove

randomRecord :: IO Record
randomRecord = do
    s <- genScramble 10
    t <- randomRIO (0,100) :: IO Float
    return $ Record t 0 s

lastRecord :: FilePath -> IO (Maybe Record)
lastRecord f = do
    rs <- readRecord f
    return $ either (const Nothing) listToMaybe rs

lastSolveNum :: FilePath -> IO Int
lastSolveNum f = do
    mRec <- lastRecord f
    case mRec of
        Just r -> return $ number r
        Nothing -> return 0

writeRecord :: FilePath -> Record -> IO ()
writeRecord f r = do
    rs <- readRecord f
    n <- lastSolveNum f
    let n' = n+1
    case rs of
        Left _ -> B.writeFile f $ encode [ r {number = n'} ]
        Right records -> do
            let r' = r {number = n'}
            B.writeFile f $ encode (r':records)

readRecord :: FilePath -> IO (Either String [Record])
readRecord f = eitherDecode <$> B.readFile f
