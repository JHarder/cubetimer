CubeTimer
=========
Cubetimer arose out of a need for an offline timer to time myself when solving Rubik's cubes.
It will function like any (computer based) timer: space to start the timer, space again to stop it
and record the speed and any other metadata.

### Done
- Record datatype including scramble generator
- json reader/writer with record datum
- pretty printer for Record type
- solve number to dynamically find last solve number and increment


### Working on
- add actual timer support (netwire?)
- command line options for which files to use,
      clearing json file, exporting to other formats
- statistics to analyize trends in solves calculated from JSON data
