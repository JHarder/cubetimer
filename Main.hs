module Main where

import Control.Monad (unless,void)
import System.Environment
import System.Directory
import System.Process (system)
import Data.Maybe (listToMaybe)

import Record
import Test (runJSONTest)

main :: IO ()
main = do
    args <- getArgs
    let f = "scores.json"
        a = listToMaybe args
    case a of
        Nothing -> go f
        Just "reset" -> void $ system "rm scores.json"
        Just _ -> go f

  where go :: FilePath -> IO ()
        go f = do
            scoreSheetExists <- doesFileExist f
            unless scoreSheetExists (void $ system $ "touch " ++ f)
            r <- randomRecord
            runJSONTest f r
